<?php

include_once("../vendor/autoload.php");

use App\Students;

$student = new Students();

if(isset($_POST['submit']))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $created_at = date("Y-m-d h:i:s", time()) ;
//$table = $_POST['table'];
    $table = "students";
    $r = $student->insertData($name, $email, $phone, $created_at, $table);

    if($r){
        header("Location:index.php");
    }
    else
        echo "Connection error";
}
