<?php
include_once("../vendor/autoload.php");

use App\Students;

$students = new Students();
extract($_POST);
$updated_at = date("Y-m-d h:i:s", time()) ;
$table = "students";
$r = $students->update($id, $name, $email, $phone, $updated_at, $table );

if($r)
   header("Location:index.php");