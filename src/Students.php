<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 7/4/2019
 * Time: 2:35 PM
 */

namespace App;


class Students extends Database
{
  private $id;
  private $name;
  private $email;
  private $phone;

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    public function insertData($name,$email,$phone, $created_at, $table){

        $sql = "INSERT INTO $table SET name=:name,email=:email,phone=:phone, created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':name'=>$name,':email'=>$email,
            ':phone'=>$phone,':created_at'=>$created_at));
        if($q)
        return true;
        else
            return false;
    }

    public function update($id,$name,$email,$phone, $updated_at, $table){

        $sql = "UPDATE $table
 SET name=:name,email=:email,phone=:phone, updated_at=:updated_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':name'=>$name,
            ':email'=>$email,':phone'=>$phone, ':updated_at'=>$updated_at ));
        return true;

    }


}