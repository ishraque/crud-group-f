<?php
include_once("../vendor/autoload.php");

use App\Students;

$students = new Students();
$id = $_GET['id'];
$table = "students";
$r = $students->deleteData($id, $table);

if($r)
   header("Location:index.php");